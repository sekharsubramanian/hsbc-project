
public class Arrays {

    public static void main(String[] args) {
        // array are objects and are stored in the heap memory
        // primitive data types are stored in stack memory
        // int [] numbers = new int [10];
        // int[] numbers = new int[] { 34, 45, 33, 11 };

        /*
         * Properties of an array 1. Fixed size 2. Contigous block of memory 3.
         * Homogenous elements 4. index starts at 0 5. Has only length property 6.
         * Default elements will be based on the data type numbers - 0 char = '' boolean
         * = false 7. Can be initialized with default values explicitly in which case
         * the array will be inialized with the elements passed using the {}
         */
        int[][] numbers = { { 34 }, { 45, 11 }, { 33, 33 }, { 21, 11 } };

        for (int index = 0; index < numbers[0].length; index++) {
            System.out.printf("The value at index  %d  is %d \n", index, numbers[0][index]);
        }

        for (int value : numbers[0]) {
            System.out.println("Value " + value);
        }
    }
}