package day1;

public class Variable {

    public static void main(String[] args) {
        //variable names
        //rules
        /*
         1. Variable names can be alphanumeric
         2. Only _ and $ are allowed 
         3. You cannot start a variable with a digit
         4. You cannot use a reserve/keyword 
         6. Java is case sensitive A != a 
        */

        //conventions
        /*
         1. Use descriptive variable names 
             do not use variable names like i, j, k 
         2. Use camelCase 
         3. Avoid using _ and $     
        */
    }
}