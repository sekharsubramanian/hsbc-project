public class Construct {

    public static void main(String[] args) {
        //operators
        //Arithmetic 
        int sum = 56 + 67;
        int product = 56 * 45;
        int modulo = 56 % 10;
        int divide = 45 / 5;

        //logical - Lazy evaluation
        boolean flag = true || false;
        flag = true && false;

        //logical 
        flag = true | false;
        flag = true & false;

        //Conditional
        boolean result = 45 > 78;
        result = 45 >= 78;
        result = 45 == 78;
        result = 45 != 78;
        result = 45 <= 78;
        result = 45 < 78;
    }
}