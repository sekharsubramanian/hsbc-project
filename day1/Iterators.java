public class Iterators {
    public static void main(String[] args) {
        // loops
        for (int index = 0; index < 10; index++) {
            System.out.println("Value is " + index);
        }

        // while loop
        int i = 0;
        while (i < 10) {
            System.out.println("Value of i is " + i);
            i++;
        }

        do {
            System.out.println("Value of i is " + i);
            i--;
        } while (i >= 0);

        // skip the current execution

        for (int index = 0; index < 10; index++) {
            if (index == 5) {
                continue;
            }
            System.out.println("Value is " + index);
        }

        // break the loop
        inner: for (int j = 0; i < 15; j++) {
            outer: for (int index = 0; index < 10; index++) {
                if (index == 5) {
                    break inner;
                }
                System.out.println("Value is " + index);
            }
        }

    }
}