public class SavingsAccount {

    //static variables 
    private static long accountNumberTracker = 1000;

    // instance variables
    private long accountNumber;

    private double accountBalance;

    private String customerName;

    private String street;

    private String city;

    private int zipCode;

    private String emailAddress;

    private String nominee;

    private String nationality;

    public long getAccountNumber() {
        return accountNumber;
    }

    /*
     * Rules of constructor: a. Name of the constructor should be the same as the
     * class name b. The constructot should not have a return type c. If you do not
     * provide a custome constructor, the compiler will create a default no argument
     * constructor c. If you provide an explict constructor, the compiler will not
     * create a default constructor
     * 
     */
    public SavingsAccount(String customerName, double initialAccountBalance) {
        this.customerName = customerName;
        this.accountBalance = initialAccountBalance;
        this.nationality = "INDIAN";
        this.accountNumber = ++ accountNumberTracker;
    }

    public SavingsAccount(String customerName, double initialAccountBalance, String nominee) {
        this.customerName = customerName;
        this.accountBalance = initialAccountBalance;
        this.nominee = nominee;
        this.nationality = "INDIAN";
        this.accountNumber = ++ accountNumberTracker;
    }

    public SavingsAccount(String customerName, double initialAccountBalance, String street, String city, int zipCode) {
        this.customerName = customerName;
        this.accountBalance = initialAccountBalance;
        this.street = street;
        this.city = city;
        this.zipCode = zipCode;
        this.nationality = "INDIAN";
        this.accountNumber = ++ accountNumberTracker;
    }

    public SavingsAccount(String customerName, double initialAccountBalance, String nominee, String nationality) {
        this.customerName = customerName;
        this.accountBalance = initialAccountBalance;
        this.nominee = nominee;
        this.nationality = nationality;
        this.accountNumber = ++ accountNumberTracker;
    }

    // instance methods
    public double withdraw(double amount) {
        if (this.accountBalance >= amount) {
            this.accountBalance = this.accountBalance - amount;
            return amount;
        }
        return 0;
    }

    public double checkBalance() {
        return this.accountBalance;
    }

    public double deposit(double amount) {
        this.accountBalance = accountBalance + amount;
        return accountBalance;
    }

    public void updateAddress(String streetName, int zipCode, String city) {

        this.street = streetName;
        this.zipCode = zipCode;
        this.city = city;
    }

    public String getCustomerName() {
        return this.customerName;
    }

}