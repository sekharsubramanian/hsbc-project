
public class SavingsAccountClient {

    public static void main(String[] args) {
        // data type variable name = new data type;
        SavingsAccount rajeesh = new SavingsAccount("Rajeesh", 10_000);
        System.out.println("Customer Name: " + rajeesh.getCustomerName());
        System.out.println("Account Number " + rajeesh.getAccountNumber());
        System.out.println("Initial Account balance " + rajeesh.checkBalance());
        System.out.println("Account Balance " + rajeesh.deposit(5000));
        System.out.println("Balance after deposit" + rajeesh.checkBalance());
        rajeesh.withdraw(200);
        System.out.println("Balance after Withdraw " + rajeesh.checkBalance());

        System.out.println(" ---------------------------------");

        SavingsAccount naveen = new SavingsAccount("Naveen", 50_000, "Disha");
        System.out.println("Account Number " + naveen.getAccountNumber());
        System.out.println("Customer Name: " + naveen.getCustomerName());
        System.out.println("Initial Account balance " + naveen.checkBalance());
        System.out.println("Account Balance " + naveen.deposit(4000));
        System.out.println("Balance after deposit" + naveen.checkBalance());
        naveen.withdraw(2000);
        System.out.println("Balance after Withdraw " + naveen.checkBalance());

        System.out.println(" ---------------------------------");

        SavingsAccount vinay = new SavingsAccount("Vinay", 34_000, "8th Ave", "Bangalore", 577142);
        System.out.println("Customer Name: " + vinay.getCustomerName());
    }
}